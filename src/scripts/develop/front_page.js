var mramor_paralax=function(params){
    var defaults = {
        block : '.-mramor_paralax',
        items : '.-mramor_paralax_item'
    };

    var options = $.extend({}, defaults, params);
    $(options.block).each(function(){
        $(this).find(options.items).each(function(){
            var pos_x=$(this).data('x');
            var pos_y=$(this).data('y');
            var z_index=$(this).data('z-index');
            var y_range=$(this).data('y-range');

            var invert=parseInt($(this).data('invert')) ? 1 : -1;
            $(this).css('left',pos_x);
            $(this).css('top',pos_y);
            var this_top=parseInt($(this).css('top').replace('px'));
            console.log(this_top);
            $(this).css('z-index',z_index);

            var this_element=this;
            var set_position=function(){
                var el_height= $(this_element).outerHeight();
                var window_height=$(window).height();
                var element_top=$(this_element).offset().top;
                // console.log(element_top);
                var scroll_top=$(window).scrollTop();

                //console.log(element_top+'_top');

                if((element_top+el_height - scroll_top)>0 && (element_top - scroll_top)<=window_height){
                    var to_top=(element_top+el_height - scroll_top);
                    var new_pos_y=(window_height-to_top)/window_height;
                    new_pos_y= parseInt(new_pos_y*y_range);
                    new_pos_y=this_top+new_pos_y*invert;

                    $(this_element).css('top',new_pos_y+'px');
                }
            }
            set_position();
            $(document).scroll(function(){
                set_position();
            });
            $(window).resize(function(){
                set_position();
            });
            $(window).load(function(){
                set_position();
            });
        });
    });
}
var slider_type_fiendsFlag,
reinitFrieendsSliderSettings = function(sliderType){
    if($(window).width()<1264 && !slider_type_fiendsFlag){
        slider_type_fiends({
            lister_position: 3,
            views_count:3
        },sliderType);
        slider_type_fiendsFlag = true;
        console.log(1);
    }else if($(window).width()>1264 && slider_type_fiendsFlag){
        slider_type_fiends({
            lister_position: 4,
            views_count:7
        },sliderType);
        slider_type_fiendsFlag=false;
            console.log(2);
    }
};



$(document).ready(function(){
    //console.log($('.-lister_type_main_template').html());
    //$('.-lister_type_main_template').innerHTML;
    setTimeout(function(){
        mramor_paralax();
    },2000);

    slider_type_main();
    slider_type_project();
    slider_type_news();
    var friends ;
    console.log(friends);
    if($(window).width()<1264){
        slider_type_fiendsFlag =true;
        friends = slider_type_fiends({
            lister_position: 3,
            views_count:3
        })
    }else if($(window).width()>=1264){
        slider_type_fiendsFlag =false;
        friends = slider_type_fiends({
            lister_position: 4,
            views_count:7
        })
    }


    $(window).resize(function(){
        reinitFrieendsSliderSettings(friends);

    });
});
