 function slider_type_main(params){
        var defaults = {
            block : '.-slider_type_main',
            items_selector: '.-item',
            title: '.-title',
            description: '.-description',
            btn_content: '.-btn_content',
            main_image:'.-main_image',
            small_image:'.-small_image',
            mobile_image:'.-mobile_image',
            template: '.-slider_type_main_template'
        };
        
        var options = $.extend({}, defaults, params);
        
        $(defaults.block).each(function(){
            var status ={
                current_slide:1
            }
            var animate_status={
                img_desktop: 1,
                img_mobile: 1,
                small_image:1,
                title:1,
                description:1,
                button:1
            }
            var lister=null;
            var slides=[];
            var this_block=this;
            var items =$(this).find(options.items_selector);
            $(items).each(function(){
                slides.push({
                    title:{
                        content:$(this).find(options.title).html(),
                        anim_in_desc:$(this).find(options.title).data('desc-in-anim'),
                        anim_out_desc:$(this).find(options.title).data('desc-out-anim'),
                        anim_in_mob:$(this).find(options.title).data('in-mob-anim'),
                        anim_out_mob:$(this).find(options.title).data('out-mob-anim')
                    },
                    description:{
                        content:$(this).find(options.description).html(),
                        anim_in_desc:$(this).find(options.description).data('desc-in-anim'),
                        anim_out_desc:$(this).find(options.description).data('desc-out-anim'),
                        anim_in_mob:$(this).find(options.description).data('in-mob-anim'),
                        anim_out_mob:$(this).find(options.description).data('out-mob-anim')
                    },
                    main_image:{
                        content:$(this).find(options.main_image).attr('src'),
                        anim_in_desc:$(this).find(options.main_image).data('desc-in-anim'),
                        anim_out_desc:$(this).find(options.main_image).data('desc-out-anim'),
                        anim_in_mob:$(this).find(options.main_image).data('in-mob-anim'),
                        anim_out_mob:$(this).find(options.main_image).data('out-mob-anim')
                    } ,
                    mobile_image:{
                        content:$(this).find(options.mobile_image).attr('src'),
                        anim_in_desc:$(this).find(options.mobile_image).data('desc-in-anim'),
                        anim_out_desc:$(this).find(options.mobile_image).data('desc-out-anim'),
                        anim_in_mob:$(this).find(options.mobile_image).data('in-mob-anim'),
                        anim_out_mob:$(this).find(options.mobile_image).data('out-mob-anim')
                    } ,
                    small_image:{
                        content:$(this).find(options.small_image).attr('src'),
                        anim_in_desc:$(this).find(options.small_image).data('desc-in-anim'),
                        anim_out_desc:$(this).find(options.small_image).data('desc-out-anim'),
                        anim_in_mob:$(this).find(options.small_image).data('in-mob-anim'),
                        anim_out_mob:$(this).find(options.small_image).data('out-mob-anim')
                    } ,
                    btn_content:{
                        href:$(this).find(options.btn_content).attr('href'),
                        text:$(this).find(options.btn_content).html(),
                        anim_in_desc:$(this).find(options.btn_content).data('desc-in-anim'),
                        anim_out_desc:$(this).find(options.btn_content).data('desc-out-anim'),
                        anim_in_mob:$(this).find(options.btn_content).data('in-mob-anim'),
                        anim_out_mob:$(this).find(options.btn_content).data('out-mob-anim')
                    }
                });             
            });
            var set_slide=function(index){
                $(this_block).find('.small_image_container img').not('.clone').attr('src',slides[index]['small_image']['content']);
                $(this_block).find('.main_image_container img.desktop').not('.clone').attr('src',slides[index]['main_image']['content']);
                $(this_block).find('.main_image_container img.mobile').not('.clone').attr('src',slides[index]['mobile_image']['content']);
            }
            $(this).html($(defaults.template).html());
            
            var test_animate=function(){
                var count=0;
                for(var i in animate_status){
                    if(animate_status[i]<1) count++;
                }
                if(!count){
                    lister.set_inprogress(0);
                }else{
                    lister.set_inprogress(1);
                }
            }
            
            var animate_out=function(flag,in_animate,out_animate){
                switch(flag){
                    case 'img_desktop':
                        if($(this_block).find('.main_image_container img.desktop').is(":visible")){
                            animate_status.img_desktop--;
                            test_animate();
                            var clone =$(this_block).find('.main_image_container img.desktop').clone().addClass('clone');
                            $(this_block).find('.main_image_container').append($(clone));
                            set_slide(status.current_slide-1);
                            $(this_block).find('.main_image_container img.desktop.clone').addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.main_image_container img.desktop.clone').remove();
                                animate_status.img_desktop++;
                                test_animate();
                            });
                            animate_in(flag,in_animate,out_animate);
                        }
                        break;
                    case 'img_mobile':
                        if($(this_block).find('.main_image_container img.mobile').is(":visible")){
                            animate_status.img_mobile--;
                            test_animate();
                            var clone =$(this_block).find('.main_image_container img.mobile').clone().addClass('clone');
                            $(this_block).find('.main_image_container').append($(clone));
                            set_slide(status.current_slide-1);
                            $(this_block).find('.main_image_container img.mobile.clone').addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.main_image_container img.mobile.clone').remove();
                                animate_status.img_mobile++;
                                test_animate();
                            });
                            animate_in(flag,in_animate,out_animate);
                        }
                        break;
                    case 'small_image':
                        if($(this_block).find('.small_image_container img').is(":visible")){
                            animate_status.small_image--;
                            test_animate();
                            var clone =$(this_block).find('.small_image_container img').clone().addClass('clone');
                            $(this_block).find('.small_image_container').append($(clone));
                            set_slide(status.current_slide-1);
                            $(this_block).find('.small_image_container img.clone').addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.small_image_container img.clone').remove();
                                animate_status.small_image++;
                                test_animate();
                            });
                            animate_in(flag,in_animate,out_animate);
                        }
                        break;
                    case 'title':
                        if($(this_block).find('.title_container').is(":visible")){
                            animate_status.title--;
                            test_animate();
                            $(this_block).find('.title_container').wrapInner('<div class="animate_wrapper"></div');
                            var clone =$(this_block).find('.title_container .animate_wrapper').clone().addClass('clone');
                            $(this_block).find('.title_container').append($(clone));
                            $(this_block).find('.title_container .animate_wrapper').not('.clone').html(slides[status.current_slide-1]['title']['content']);
                            $(this_block).find('.title_container .animate_wrapper.clone').addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.title_container .animate_wrapper.clone').remove();
                                animate_status.title++;
                                test_animate();

                            });
                            animate_in(flag,in_animate,out_animate);
                        }
                        break;
                    case 'description':
                        if($(this_block).find('.description_container').is(":visible")){
                            animate_status.description--;
                            test_animate();
                            $(this_block).find('.description_container').wrapInner('<div class="animate_wrapper"></div');
                            var clone =$(this_block).find('.description_container .animate_wrapper').clone().addClass('clone');
                            $(this_block).find('.description_container').append($(clone));
                            $(this_block).find('.description_container .animate_wrapper').not('.clone').html(slides[status.current_slide-1]['description']['content']);
                            $(this_block).find('.description_container .animate_wrapper.clone').addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.description_container .animate_wrapper.clone').remove();
                                animate_status.description++;
                                test_animate();

                            });
                            animate_in(flag,in_animate,out_animate);
                        }
                        break;
                    case 'button':
                        if($(this_block).find('.btn_content_container a').is(":visible")){
                            animate_status.button--;
                            test_animate();
                            var clone =$(this_block).find('.btn_content_container a').clone().addClass('clone');
                            $(this_block).find('.btn_content_container').append($(clone));
                            set_slide(status.current_slide-1);
                            $(this_block).find('.btn_content_container a.clone').addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.btn_content_container a.clone').remove();
                                animate_status.button++;
                                test_animate();

                            });
                            animate_in(flag,in_animate,out_animate);
                        }
                    break;
                }
                
            }
            var animate_in=function(flag,in_animate,out_animate){
                
                switch(flag){
                    case 'img_desktop':
                        if($(this_block).find('.main_image_container img.desktop').not('.clone').is(":visible")){
                            animate_status.img_desktop--;
                            $(this_block).find('.main_image_container img.desktop').not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.main_image_container img.desktop').not('.clone').removeClass(in_animate+' animated');
                                animate_status.img_desktop++;
                                test_animate();
                            });
                        }
                        break;
                    case 'img_mobile':
                        if($(this_block).find('.main_image_container img.mobile').not('.clone').is(":visible")){
                            animate_status.img_mobile--;
                            $(this_block).find('.main_image_container img.mobile').not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.main_image_container img.mobile').not('.clone').removeClass(in_animate+' animated');
                                animate_status.img_mobile++;
                                test_animate();
                            });
                        }
                        break;
                    case 'small_image':
                        if($(this_block).find('.small_image_container img').not('.clone').is(":visible")){
                            animate_status.small_image--;
                            $(this_block).find('.small_image_container img').not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.small_image_container img').not('.clone').removeClass(in_animate+' animated');
                                animate_status.small_image++;
                                test_animate();

                            });
                        }
                        break;
                    case 'title':
                        if($(this_block).find('.title_container .animate_wrapper').not('.clone').is(":visible")){
                            animate_status.title--;
                            $(this_block).find('.title_container .animate_wrapper').not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.title_container').html($(this_block).find('.title_container .animate_wrapper').not('.clone').html());

                                //$(this_block).find('.title_container .animate_wrapper').not('.clone').removeClass('fadeInLeft animated');
                                //$(this_block).find('.title_container .animate_wrapper').not('.clone').unwrap();
                                animate_status.title++;
                                test_animate();
                            });
                        }
                        break;
                    case 'description':
                        if($(this_block).find('.description_container .animate_wrapper').not('.clone').is(":visible")){
                            animate_status.description--;
                            $(this_block).find('.description_container .animate_wrapper').not('.clone')
                                    .addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.description_container').html($(this_block).find('.description_container .animate_wrapper').not('.clone').html());
                                animate_status.description++;
                                test_animate();
                            });
                        }
                        break;
                    case 'button':
                        if($(this_block).find('.btn_content_container a').not('.clone').is(":visible")){
                            animate_status.button--;
                            $(this_block).find('.btn_content_container a').not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                $(this_block).find('.btn_content_container a').not('.clone').removeClass(in_animate+' animated');
                                animate_status.button++;
                                test_animate();
                            });
                        }
                        break;
                }
            }
            var change_slide=function(move){
                if(move=='prev'){
                    if(status.current_slide>1){
                        status.current_slide--;
                    }else{
                        status.current_slide=slides.length
                    }
                }else{
                    if(status.current_slide<slides.length){
                        status.current_slide++;
                    }else{
                        status.current_slide=1
                    }
                }
                if(!$('html').hasClass('mobile')){
                    animate_out('img_desktop',slides[status.current_slide-1]['main_image']['anim_in_desc'],slides[status.current_slide-1]['main_image']['anim_out_desc']);
                    animate_out('small_image',slides[status.current_slide-1]['small_image']['anim_in_desc'],slides[status.current_slide-1]['small_image']['anim_out_desc']);
                    animate_out('title',slides[status.current_slide-1]['title']['anim_in_desc'],slides[status.current_slide-1]['title']['anim_out_desc']);
                    animate_out('description',slides[status.current_slide-1]['description']['anim_in_desc'],slides[status.current_slide-1]['description']['anim_out_desc']);
                    animate_out('button',slides[status.current_slide-1]['btn_content']['anim_in_desc'],slides[status.current_slide-1]['btn_content']['anim_out_desc']);
                }else{
                    animate_out('img_mobile',slides[status.current_slide-1]['mobile_image']['anim_in_mob'],slides[status.current_slide-1]['mobile_image']['anim_out_mob']);
                    animate_out('title',slides[status.current_slide-1]['title']['anim_in_mob'],slides[status.current_slide-1]['title']['anim_out_mob']);
                    animate_out('description',slides[status.current_slide-1]['description']['anim_in_mob'],slides[status.current_slide-1]['description']['anim_out_mob']);
                    animate_out('button',slides[status.current_slide-1]['btn_content']['anim_in_mob'],slides[status.current_slide-1]['btn_content']['anim_out_mob']);
                }
                
                
            }
            var prev=function(e){
                e.set_inprogress(1);
                change_slide('prev');
            };
            var next=function(e){
                e.set_inprogress(1);
                change_slide('next');
            };
           var  init=function(){ 
               if(slides.length>0){
                    $(this_block).find('.title_container').not('.clone').html(slides[0]['title']['content']);
                    $(this_block).find('.description_container').not('.clone').html(slides[0]['description']['content']);
                    $(this_block).find('.btn_content_container a').not('.clone').attr('href',slides[0]['btn_content']['href']);
                    $(this_block).find('.btn_content_container a').not('.clone').html(slides[0]['btn_content']['text']);
                    set_slide(0);
                    console.log(slides);
                    lister=lister_type_main({
                        count:slides.length,
                        template: $('.-lister_type_main_template').html(),
                        block: $(this_block).find('.lister_container'),
                        CallBackPrev: prev,
                        CallBackNext: next
                    });
                }
            }
            init();
        });
     }
