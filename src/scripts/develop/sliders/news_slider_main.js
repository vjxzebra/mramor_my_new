 function slider_type_news(params){
        var defaults = {
            block : '.-slider_news_main',
            items_selector: '.-item',
            template:'.-news_slider_template',
            title: '.-title',
            description: '.-description',
            news_link: '.-news_link a',
            news_image:'.-news_image img',
            date:'.-date',
            reviews:'.-reviews',
            start_index:0,
            views_count:2,
            animation:{
                content_animation_left:'.-content_animation_left',
                img_animation_left:'.-img_animation_left',
                content_animation_right:'.-content_animation_right',
                img_animation_right:'.-img_animation_right'
            }
        };

        var options = $.extend({}, defaults, params);

        $(defaults.block).each(function(){
            var status ={
                current_slide:options.start_index,
                count_slides:0
            }
            var animate_status={
                img_animation: 1,
                content_animation: 1
            }
            var lister=null;
            var slides=[];
            var slider_array=[];
            var this_block=this;
            var items =$(this).find(options.items_selector);
            //title
            var set_title=function(slide_index,el_index){
                var element=$(this_block).find('.-slide_wrapper').eq(el_index);
                $(element).find('.-title').not('.clone').html(slider_array[slide_index][el_index]['title']['html']);
            }

            //description
            var set_description=function(slide_index,el_index){
                var element=$(this_block).find('.-slide_wrapper').eq(el_index);
                $(element).find('.-description').not('.clone').html(slider_array[slide_index][el_index]['description']['html']);
            }

            //news_link
            var news_link=function(slide_index,el_index){
                var element=$(this_block).find('.-slide_wrapper').eq(el_index);
                $(element).find('.-news_link').not('.clone').attr('href',slider_array[slide_index][el_index]['news_link']['href']);
                $(element).find('.-news_link').not('.clone').attr('title',slider_array[slide_index][el_index]['news_link']['title']);
                $(element).find('.-news_link').not('.clone').html(slider_array[slide_index][el_index]['news_link']['html']);
            }

            //img_wrapper
            var set_img=function(slide_index,el_index){
                var element=$(this_block).find('.-slide_wrapper').eq(el_index);
                $(element).find('.-img_wrapper img').not('.clone').attr('src',slider_array[slide_index][el_index]['news_image']['src']);
                $(element).find('.-img_wrapper img').not('.clone').attr('title',slider_array[slide_index][el_index]['news_image']['title']);
            }

            //reviews
            var set_reviews=function(slide_index,el_index){
                var element=$(this_block).find('.-slide_wrapper').eq(el_index);
                $(element).find('.-reviews').not('.clone').html(slider_array[slide_index][el_index]['reviews']['html']);
            }
            //date
            var set_date=function(slide_index,el_index){
                var element=$(this_block).find('.-slide_wrapper').eq(el_index);
                $(element).find('.-date').not('.clone').html(slider_array[slide_index][el_index]['date']['html']);
            }
            var set_content = function(slide_index,el_index){

                set_title(slide_index,el_index);
                set_description(slide_index,el_index);
                news_link(slide_index,el_index);
                set_reviews(slide_index,el_index);
                set_date(slide_index,el_index);
            }
            var tmp_index=0;
            $(items).each(function(){
                var content_animation={};
                var img_animation={}
                var content_animation={
                    desc_in_anim_left:$(this).find(options.animation.content_animation_left).data('desc-in-anim'),
                    desc_out_anim_left:$(this).find(options.animation.content_animation_left).data('desc-out-anim'),
                    in_mob_anim_left:$(this).find(options.animation.content_animation_left).data('in-mob-anim'),
                    out_mob_anim_left:$(this).find(options.animation.content_animation_left).data('out-mob-anim'),
                    desc_in_anim_right:$(this).find(options.animation.content_animation_right).data('desc-in-anim'),
                    desc_out_anim_right:$(this).find(options.animation.content_animation_right).data('desc-out-anim'),
                    in_mob_anim_right:$(this).find(options.animation.content_animation_right).data('in-mob-anim'),
                    out_mob_anim_right:$(this).find(options.animation.content_animation_right).data('out-mob-anim')
                };
                var img_animation={
                    desc_in_anim_left:$(this).find(options.animation.img_animation_left).data('desc-in-anim'),
                    desc_out_anim_left:$(this).find(options.animation.img_animation_left).data('desc-out-anim'),
                    in_mob_anim_left:$(this).find(options.animation.img_animation_left).data('in-mob-anim'),
                    out_mob_anim_left:$(this).find(options.animation.img_animation_left).data('out-mob-anim'),
                    desc_in_anim_right:$(this).find(options.animation.img_animation_right).data('desc-in-anim'),
                    desc_out_anim_right:$(this).find(options.animation.img_animation_right).data('desc-out-anim'),
                    in_mob_anim_right:$(this).find(options.animation.img_animation_right).data('in-mob-anim'),
                    out_mob_anim_right:$(this).find(options.animation.img_animation_right).data('out-mob-anim')
                }
                slides.push({
                    title:{
                        html:$(this).find(options.title).html()
                    },
                    description:{
                        html:$(this).find(options.description).html()
                    },
                    news_link:{
                        href:$(this).find(options.news_link).attr('href'),
                        html:$(this).find(options.news_link).html(),
                        title:$(this).find(options.news_link).attr('title')
                    } ,
                    news_image:{
                        src:$(this).find(options.news_image).attr('src'),
                        title:$(this).find(options.news_image).attr('title')
                    },
                    date:{
                        html:$(this).find(options.date).html()
                    },
                    reviews:{
                        html:$(this).find(options.reviews).html()
                    },
                    animation:{
                        content_animation:content_animation,
                        img_animation:img_animation
                    }
                });
                tmp_index++;
            });


            //Формирование массива слайдов
            var tmp_index_views=options.views_count;
            var tmp_array=[];
            $(slides).each(function(){
                if(tmp_index_views<=0){
                    slider_array.push(tmp_array);
                    tmp_array=[];
                    tmp_index_views=options.views_count;
                }
                tmp_index_views--;
                tmp_array.push(this);
            });
            //End Формирование массива слайдов
            var count_slides_tmp=slides.length;
            for(var i=0;i<tmp_index_views;i++){
                tmp_array.push(slides[slides.length-count_slides_tmp]);
                if(count_slides_tmp>1){
                    count_slides_tmp--;
                }else{
                    count_slides_tmp=slides.length;

                }
            }
            slider_array.push(tmp_array);

            //HTML generation


            $(this).html($(options.template).html());
            var item_temlate= $(this).find('.-news_slider_template').html();
            var res_html='';
            for(var i=0;i<options.views_count;i++){
                res_html+=item_temlate;
            }
            $(this).find('.-news_slider_template').html(res_html);
            //END HTML generation


            var test_animate=function(){
                var count=0;
                for(var i in animate_status){
                    if(animate_status[i]<1) count++;
                }
                if(!count){
                    lister.set_inprogress(0);
                }else{
                    lister.set_inprogress(1);
                }
            }

            var animate_out=function(flag,in_animate,out_animate,el_index){
                var side='_left';
                if(el_index%2){side='_right'}
                switch(flag){
                    case 'img_animation':
                        animate_status.img_animation--;
                        test_animate();
                        $(this_block).find('.-img_wrapper').eq(el_index).addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                        function(){
                            set_img(status.current_slide,el_index);
                            animate_status.img_animation++;
                            test_animate();
                            $(this_block).find('.-img_wrapper').eq(el_index).not('.clone').removeClass(out_animate+' animated');
                            animate_in(flag,in_animate,out_animate,el_index);
                            var in_anim=slides[status.current_slide]['animation']['content_animation']['desc_in_anim'+side];
                            var out_anim=slides[status.current_slide]['animation']['content_animation']['desc_out_anim'+side];
                            if($('html').hasClass('mobile')){
                                in_anim=slides[status.current_slide]['animation']['content_animation']['in_mob_anim'+side];
                                out_anim=slides[status.current_slide]['animation']['content_animation']['out_mob_anim'+side];
                            }
                            animate_out('content_animation',in_anim,out_anim,el_index);
                        });
                        break;
                    case 'content_animation':
                        animate_status.content_animation--;
                        test_animate();
                        $(this_block).find('.-content_wrapper').eq(el_index).addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                        function(){
                            set_content(status.current_slide,el_index);
                            animate_status.content_animation++;
                            test_animate();
                            $(this_block).find('.-content_wrapper').eq(el_index).not('.clone').removeClass(out_animate+' animated');
                            animate_in(flag,in_animate,out_animate,el_index);
                        });
                        break;
                }

            }
            var animate_in=function(flag,in_animate,out_animate,el_index){

                switch(flag){
                    case 'img_animation':
                        animate_status.img_animation--;
                        $(this_block).find('.-img_wrapper').eq(el_index).not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                        function(){
                            $(this_block).find('.-img_wrapper').eq(el_index).not('.clone').removeClass(in_animate+' animated');
                            animate_status.img_animation++;
                            test_animate();
                        });
                        break;
                    case 'content_animation':
                        animate_status.content_animation--;

                        $(this_block).find('.-content_wrapper').eq(el_index).not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                        function(){

                            $(this_block).find('.-content_wrapper').eq(el_index).not('.clone').removeClass(in_animate+' animated');
                            animate_status.content_animation++;
                            test_animate();
                        });
                        break;
                }
            }
            var change_slide=function(move){
                if(move=='prev'){
                    if(status.current_slide>0){
                        status.current_slide--;
                    }else{
                        status.current_slide=slider_array.length-1
                    }
                }else{
                    if(status.current_slide<slider_array.length-1){
                        status.current_slide++;
                    }else{
                        status.current_slide=0
                    }
                }

                for(var i=0; i<options.views_count;i++){
                    var side='_left';
                    if(i%2){side='_right'};
                    if(!$('html').hasClass('mobile')){
                        animate_out('img_animation',slides[status.current_slide]['animation']['img_animation']['desc_in_anim'+side],slides[status.current_slide]['animation']['img_animation']['desc_out_anim'+side],i);
                    }else{
                        animate_out('img_animation',slides[status.current_slide]['animation']['img_animation']['in_mob_anim'+side],slides[status.current_slide]['animation']['img_animation']['out_mob_anim'+side],i);
                    }
                }
            }
            var prev=function(e){
                e.set_inprogress(1);
                change_slide('prev');
            };
            var next=function(e){
                e.set_inprogress(1);
                change_slide('next');
            };
            var render_base=function(){
                for(var i=0;i<options.views_count;i++){
                    set_content(status.current_slide,i);
                    set_img(status.current_slide,i);

                }
            }
           var  init=function(){
               if(slides.length>0){
                    status.count_slides=slider_array.length;
                    render_base();
                    console.log(slider_array);
                    lister=lister_type_main({
                        count:status.count_slides,
                        template: $('.-lister_type_news_template').html(),
                        block: $(this_block).find('.-lister_container'),
                        CallBackPrev: prev,
                        CallBackNext: next
                    });
                }
            }
            init();
        });
     }
