 function lister_type_main(params){
    var defaults = {
       count : 0,
       start: 1,
       block: '',
       template:'',
       CallBackPrev:function(){},
       CallBackNext:function(){}
    };
    var options = $.extend({}, defaults, params);
    var status={
        in_progress:0,
        current_slide:options.start,
        set_status: function(){
            $(options.block).find('.-current-slide').html(status.current_slide);
            if(status.in_progress){
                $(options.block).addClass('in_progress');
            }else{
                $(options.block).removeClass('in_progress');
            }
        }
    }
    var js_flip=function(){
        if($(options.block).find('.-rotating-wraper').hasClass('js_flip')){
            $(options.block).find('.-current-slide').html((status.current_slide));
            //$(options.block).find('.-next-slide').html((status.current_slide));
        }else{
            $(options.block).find('.-next-slide').html((status.current_slide));
            //$(options.block).find('.-current-slide').html((status.current_slide));
        }
        $(options.block).find('.-rotating-wraper').toggleClass('js_flip');
    }
    var set_inprogress=function(in_pr){
        status.in_progress=in_pr;
        status.set_status();
    }
    var prev_click=function(){
        if(!status.in_progress){
            if(status.current_slide>1){
                status.current_slide--;
            }else{
                status.current_slide=options.count
            }
            status.set_status();
            js_flip();
            options.CallBackPrev(lister);
            
        }
        //console.log(status.current_slide);
    }
    var next_click =function(){
        if(!status.in_progress){
            if(status.current_slide<options.count){
                status.current_slide++;
            }else{
                status.current_slide=1
            }
            status.set_status();
            js_flip();
            options.CallBackNext(lister);
        }
        //console.log(status.current_slide);
    }
    var lister={
        set_inprogress:set_inprogress,
        prev_click:prev_click,
        next_click:next_click
    }
    
    
    var init=function(){
        var template=$(options.template);
    
        $(options.block).html(template.html());
        
        $(options.block).on('click','.prev',prev_click);
        $(options.block).on('click','.next',next_click);
        status.set_status();
        //console.log(options.count+'tyt');
        $(options.block).find('.-total-amount').html(options.count);
    }
    
    init();
    return lister;
}

