 function slider_type_project(params){
        var defaults = {
            block : '.-slider_project_main',
            items_selector: '.-item',
            template:'.-project_slider_template',
            title: '.-title',
            name_of_project:'.-name_of_project',
            description: '.-description',
            link_of_project: '.-link_of_project',
            img_project:'.-img_project img',
            start_index:0,
            animation:{
                content_animation:'.-content_animation',
                nav_btn_animation:'.-nav_btn_animation',
                img_project_animation:'.-img_project_animation'
            }
        };
        
        var options = $.extend({}, defaults, params);
        
        $(defaults.block).each(function(){
            var status ={
                current_slide:1
            }
            var animate_status={
                img_project: 1,
                content: 1,
                prev_btn:1,
                next_btn:1
            }
            var lister=null;
            var slides=[];
            var this_block=this;
            var items =$(this).find(options.items_selector);
            //title
            var set_title=function(index){
                $(this_block).find('.-title').not('.clone').html(slides[index]['title']['html']);
            }
            
            //description
            var set_description=function(index){
                $(this_block).find('.-description').not('.clone').html(slides[index]['description']['html']);
            }
            
            //link_of_project
            var set_link_of_project=function(index){
                $(this_block).find('.-link_of_project').not('.clone').attr('href',slides[index]['link_of_project']['href']);
                    $(this_block).find('.-link_of_project').not('.clone').attr('title',slides[index]['link_of_project']['title']);
                    $(this_block).find('.-link_of_project').not('.clone').html(slides[index]['link_of_project']['html']);
            }
            
            //img_project
            var set_img_project=function(index){
                $(this_block).find('.-img_project img').not('.clone').attr('src',slides[index]['img_project']['src']);
                $(this_block).find('.-img_project img').not('.clone').attr('title',slides[index]['img_project']['title']);
            }
            
            //name_of_project
            var set_name_of_project=function(index){
                $(this_block).find('.-name_of_project').not('.clone').html(slides[index]['name_of_project']['html']);
            }
            var set_content = function(index){
                set_title(index);
                set_description(index);
                set_link_of_project(index);
                set_name_of_project(index);
            }
            //prev_btn/next_btn
            var set_prev_next_btn=function(index){
                if(slides.length>=3){
                        if(index==slides.length-1){
                            $(this_block).find('.-prev_btn p').not('.clone').html(slides[index-1]['title']['html']);
                            $(this_block).find('.-next_btn p').not('.clone').html(slides[0]['title']['html']);
                        }else{
                            if(index==0){
                                $(this_block).find('.-prev_btn p').not('.clone').html(slides[slides.length-1]['title']['html']);
                                $(this_block).find('.-next_btn p').not('.clone').html(slides[index+1]['title']['html']);
                            }else{
                                $(this_block).find('.-prev_btn p').not('.clone').html(slides[index-1]['title']['html']);
                                $(this_block).find('.-next_btn p').not('.clone').html(slides[index+1]['title']['html']);
                            }
                        }
                        
                    }else{
                        if(slides.length==2){
                            $(this_block).find('.-prev_btn p').not('.clone').html(slides[1]['title']['html']);
                            $(this_block).find('.-next_btn p').not('.clone').html(slides[1]['title']['html']);
                        }else{
                            $(this_block).find('.-prev_btn p').not('.clone').html(slides[0]['title']['html']);
                            $(this_block).find('.-next_btn p').not('.clone').html(slides[0]['title']['html']);
                        }
                    }
            }
            $(items).each(function(){
                slides.push({
                    title:{
                        html:$(this).find(options.title).html()
                    },
                    description:{
                        html:$(this).find(options.description).html()
                    },
                    name_of_project:{
                        html:$(this).find(options.name_of_project).html()
                    } ,
                    link_of_project:{
                        href:$(this).find(options.link_of_project).attr('href'),
                        html:$(this).find(options.link_of_project).html(),
                        title:$(this).find(options.link_of_project).attr('title')
                    } ,
                    img_project:{
                        src:$(this).find(options.img_project).attr('src'),
                        title:$(this).find(options.img_project).attr('title')
                    },
                    animation:{
                        content_animation:{
                            desc_in_anim:$(this).find(options.animation.content_animation).data('desc-in-anim'),
                            desc_out_anim:$(this).find(options.animation.content_animation).data('desc-out-anim'),
                            in_mob_anim:$(this).find(options.animation.content_animation).data('in-mob-anim'),
                            out_mob_anim:$(this).find(options.animation.content_animation).data('out-mob-anim')
                        },
                        nav_btn_animation:{
                            desc_in_anim:$(this).find(options.animation.nav_btn_animation).data('desc-in-anim'),
                            desc_out_anim:$(this).find(options.animation.nav_btn_animation).data('desc-out-anim')
                        },
                        img_project_animation:{
                            desc_in_anim:$(this).find(options.animation.img_project_animation).data('desc-in-anim'),
                            desc_out_anim:$(this).find(options.animation.img_project_animation).data('desc-out-anim'),
                            in_mob_anim:$(this).find(options.animation.img_project_animation).data('in-mob-anim'),
                            out_mob_anim:$(this).find(options.animation.img_project_animation).data('out-mob-anim')
                        }
                    }
                });             
            });
            $(this).html($(options.template).html());
            
            var test_animate=function(){
                var count=0;
                for(var i in animate_status){
                    if(animate_status[i]<1) count++;
                }
                if(!count){
                    lister.set_inprogress(0);
                }else{
                    lister.set_inprogress(1);
                }
            }
            
            var animate_out=function(flag,in_animate,out_animate){
                switch(flag){
                    case 'content_animation':
                        animate_status.content--;
                        test_animate();
                        $(this_block).find('.-content').addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                        function(){
                            set_content(status.current_slide-1);
                            animate_status.content++;
                            test_animate();
                            $(this_block).find('.-content').not('.clone').removeClass(out_animate+' animated');
                            animate_in(flag,in_animate,out_animate);
                        });
                        break;
                    case 'nav_btn_animation':
                        animate_status.prev_btn--;
                        animate_status.next_btn--;
                        test_animate();
                        var clone =$(this_block).find('.-prev_btn p').clone().addClass('clone');
                        $(this_block).find('.-prev_btn').append($(clone));
                        
                        
                        $(this_block).find('.-prev_btn p.clone').addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                        function(){
                            $(this_block).find('.-prev_btn p.clone').remove();
                            animate_status.prev_btn++;
                            test_animate();
                        });
                        test_animate();
                        clone =$(this_block).find('.-next_btn p').clone().addClass('clone');
                        $(this_block).find('.-next_btn').append($(clone));
                        $(this_block).find('.-next_btn p.clone').addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                        function(){
                            $(this_block).find('.-next_btn p.clone').remove();
                            animate_status.next_btn++;
                            test_animate();
                        });
                        set_prev_next_btn(status.current_slide-1);
                        animate_in(flag,in_animate,out_animate);
                        break;
                    case 'img_project_animation':
                        animate_status.img_project--;
                        test_animate();
                        $(this_block).find('.-img_project').addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                        function(){
                            set_img_project(status.current_slide-1);
                            animate_status.img_project++;
                            test_animate();
                            $(this_block).find('.-img_project').not('.clone').removeClass(out_animate+' animated');
                            animate_in(flag,in_animate,out_animate);
                            
                            var in_anim=slides[status.current_slide-1]['animation']['content_animation']['desc_in_anim'];
                            var out_anim=slides[status.current_slide-1]['animation']['content_animation']['desc_out_anim'];
                            if($('html').hasClass('mobile')){
                                in_anim=slides[status.current_slide-1]['animation']['content_animation']['in_mob_anim'];
                                out_anim=slides[status.current_slide-1]['animation']['content_animation']['out_mob_anim'];
                            }
                            
                            animate_out('content_animation',in_anim,out_anim);
                        });
                        break;
                }
                
            }
            var animate_in=function(flag,in_animate,out_animate){
                
                switch(flag){
                    case 'content_animation':
                        animate_status.content--;
                        $(this_block).find('.-content').not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                        function(){
                            $(this_block).find('.-content').not('.clone').removeClass(in_animate+' animated');
                            animate_status.content++;
                            test_animate();
                        });
                        break;
                    case 'nav_btn_animation':
                        animate_status.prev_btn--;
                        animate_status.next_btn--;
                        $(this_block).find('.-prev_btn p').not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                        function(){
                            $(this_block).find('.-prev_btn p').not('.clone').removeClass(in_animate+' animated');
                            animate_status.prev_btn++;
                            test_animate();
                        });
                        $(this_block).find('.-next_btn p').not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                        function(){
                            $(this_block).find('.-next_btn p').not('.clone').removeClass(in_animate+' animated');
                            animate_status.next_btn++;
                            test_animate();
                        });
                        break;
                    case 'img_project_animation':
                        animate_status.img_project--;
                        $(this_block).find('.-img_project').not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                        function(){
                            $(this_block).find('.-img_project').not('.clone').removeClass(in_animate+' animated');
                            animate_status.img_project++;
                            test_animate();
                        });
                        break;
                }
            }
            var change_slide=function(move){
                if(move=='prev'){
                    if(status.current_slide>1){
                        status.current_slide--;
                    }else{
                        status.current_slide=slides.length
                    }
                }else{
                    if(status.current_slide<slides.length){
                        status.current_slide++;
                    }else{
                        status.current_slide=1
                    }
                }
                if(!$('html').hasClass('mobile')){
                    animate_out('nav_btn_animation',slides[status.current_slide-1]['animation']['nav_btn_animation']['desc_in_anim'],slides[status.current_slide-1]['animation']['nav_btn_animation']['desc_out_anim']);
                    animate_out('img_project_animation',slides[status.current_slide-1]['animation']['img_project_animation']['desc_in_anim'],slides[status.current_slide-1]['animation']['img_project_animation']['desc_out_anim']);
                }else{
                    animate_out('img_project_animation',slides[status.current_slide-1]['animation']['img_project_animation']['in_mob_anim'],slides[status.current_slide-1]['animation']['img_project_animation']['out_mob_anim']);
                }
                
                
            }
            var prev=function(e){
                e.set_inprogress(1);
                change_slide('prev');
            };
            $(this_block).on('click','.-prev_btn',function(){
                lister.prev_click();
            });
            var next=function(e){
                e.set_inprogress(1);
                change_slide('next');
            };
            $(this_block).on('click','.-next_btn',function(){
                lister.next_click();
            });
            
            
           var  init=function(){ 
               if(slides.length>0){
                    set_content(options.start_index);
                    
                    //img_project
                    set_img_project(options.start_index);
                    
                    //prev_btn/next_btn
                    set_prev_next_btn(options.start_index);
                    
                    console.log(slides);
                    lister=lister_type_main({
                        count:slides.length,
                        template: $('.-lister_type_project_template').html(),
                        block: $(this_block).find('.-project_lister'),
                        CallBackPrev: prev,
                        CallBackNext: next
                    });
                }
            }
            init();
        });
     }
