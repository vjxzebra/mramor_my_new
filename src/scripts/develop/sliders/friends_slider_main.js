 function slider_type_fiends(params,slides){
        var defaults = {
            block : '.-friends_slider_main',
            items_selector: '.-item',
            template:'.-friends_item_template',
            title: '.-title',
            description: '.-description',
            link: '.-link a',
            image:'.-image img',
            start_index:0,
            views_count:7,
            lister_position: 4,
            elements_delay:100,
            animation:{
                content:'.-content_animation'
            }
        };
        
        var options = $.extend({}, defaults, params);
        var base_return='';
        var base_slides=$(options.block);
        if(typeof slides == 'object'){
            $(options.block).html($(slides).html());
        }else{
            base_return=$(base_slides).clone();
        }
        
        
        var init_sliders=function(base_slides){
            $(base_slides).each(function(){
                var status ={
                    current_slide:options.start_index,
                    count_slides:0
                }
                var animate_status={
                    content_animation: 1,
                    in_cycle:1
                }
                var lister=null;
                var slides=[];
                var slider_array=[];
                var this_block=this;
                var items =$(this).find(options.items_selector);
                //title
                var set_title=function(slide_index,el_index){
                    var eq_index=el_index;
                    if(eq_index>=options.lister_position) eq_index++;
                    var element=$(this_block).find('.-item').eq(eq_index);
                    $(element).find('.-title').not('.clone').html(slider_array[slide_index][el_index]['title']['html']);
                }

                //description
                var set_description=function(slide_index,el_index){
                    var eq_index=el_index;
                    if(eq_index>=options.lister_position) eq_index++;
                    var element=$(this_block).find('.-item').eq(eq_index);
                    $(element).find('.-description').not('.clone').html(slider_array[slide_index][el_index]['description']['html']);
                }

                //news_link
                var set_link=function(slide_index,el_index){
                    var eq_index=el_index;
                    if(eq_index>=options.lister_position) eq_index++;
                    var element=$(this_block).find('.-item').eq(eq_index);
                    $(element).find('.-link').not('.clone').attr('href',slider_array[slide_index][el_index]['link']['href']);
                    $(element).find('.-link').not('.clone').attr('title',slider_array[slide_index][el_index]['link']['title']);
                    //$(element).find('.-link').not('.clone').html(slider_array[slide_index][el_index]['link']['html']);
                }

                //img_wrapper
                var set_img=function(slide_index,el_index){
                    var eq_index=el_index;
                    if(eq_index>=options.lister_position) eq_index++;
                    var element=$(this_block).find('.-item').eq(eq_index);
                    $(element).find('.-image').not('.clone').attr('src',slider_array[slide_index][el_index]['image']['src']);
                    $(element).find('.-image').not('.clone').attr('title',slider_array[slide_index][el_index]['image']['title']);
                }

                var set_content = function(slide_index,el_index){

                    set_title(slide_index,el_index);
                    set_description(slide_index,el_index);
                    set_link(slide_index,el_index);
                    set_img(slide_index,el_index);
                }
                var tmp_index=0;
                $(items).each(function(){
                    var content_animation={};
                    var content_animation={
                        desc_in_anim:$(this).find(options.animation.content).data('desc-in-anim'),
                        desc_out_anim:$(this).find(options.animation.content).data('desc-out-anim'),
                        in_mob_anim:$(this).find(options.animation.content).data('in-mob-anim'),
                        out_mob_anim:$(this).find(options.animation.content).data('out-mob-anim')
                    };
                    slides.push({
                        title:{
                            html:$(this).find(options.title).html()
                        },
                        description:{
                            html:$(this).find(options.description).html()
                        },
                        link:{
                            href:$(this).find(options.link).attr('href'),
                            html:$(this).find(options.link).html(),
                            title:$(this).find(options.link).attr('title')
                        } ,
                        image:{
                            src:$(this).find(options.image).attr('src'),
                            title:$(this).find(options.image).attr('title')
                        },
                        animation:{
                            content_animation:content_animation
                        }
                    });             
                });


                //Формирование массива слайдов
                var tmp_index_views=options.views_count;
                var tmp_array=[];

                $(slides).each(function(){
                    if(tmp_index_views<=0){
                        slider_array.push(tmp_array);
                        tmp_array=[];
                        tmp_index_views=options.views_count;

                    }
                    tmp_index_views--;
                    tmp_array.push(this);
                });

                //End Формирование массива слайдов
                var count_slides_tmp=slides.length;
                for(var i=0;i<tmp_index_views;i++){
                    if(count_slides_tmp>1){
                        count_slides_tmp--;
                    }else{
                        count_slides_tmp=slides.length;
                    }
                    tmp_array.push(slides[slides.length-count_slides_tmp]);

                }
                slider_array.push(tmp_array);
                //HTML generation
                var template_html=$(options.template).html();
                var lister_template=$('.-lister_type_friends_template').html();
                var res_html='';
                for(var i=0;i<=options.views_count;i++){
                    if(i!=options.lister_position){
                        res_html+=template_html;
                    }else{
                        res_html+='<div class="-item item -reserved reserved -lister_container lister_container">'+lister_template+'</div>';
                    }

                }
                $(this).html(res_html);
                //END HTML generation


                var test_animate=function(){
                    var count=0;
                    for(var i in animate_status){
                        if(animate_status[i]<1) count++;
                    }
                    if(!count){
                        lister.set_inprogress(0);
                    }else{
                        lister.set_inprogress(1);
                    }
                };

                var animate_out=function(flag,in_animate,out_animate,el_index){
                    var eq_index=el_index;
                    if(eq_index>=options.lister_position) eq_index++;
                    switch(flag){
                        case 'content_animation':
                            animate_status.content_animation--;
                            test_animate();
                            $(this_block).find('.-item').eq(eq_index).addClass(out_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){
                                set_content(status.current_slide,el_index);
                                animate_status.content_animation++;
                                test_animate();
                                $(this_block).find('.-item').eq(eq_index).not('.clone').removeClass(out_animate+' animated');
                                animate_in(flag,in_animate,out_animate,el_index);
                                console.log(el_index);
                            });
                            break;
                    }

                };
                var animate_in=function(flag,in_animate,out_animate,el_index){
                    var eq_index=el_index;
                    if(eq_index>=options.lister_position) eq_index++;
                    switch(flag){
                        case 'content_animation':
                            animate_status.content_animation--;

                            $(this_block).find('.-item').eq(eq_index).not('.clone').addClass(in_animate+' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                            function(){

                                $(this_block).find('.-item').eq(eq_index).not('.clone').removeClass(in_animate+' animated');
                                animate_status.content_animation++;
                                test_animate();
                            });
                            break;
                    }
                }
                var start_animation=function(i){
                    if(!$('html').hasClass('mobile')){
                        animate_out('content_animation',slides[status.current_slide]['animation']['content_animation']['desc_in_anim'],slides[status.current_slide]['animation']['content_animation']['desc_out_anim'],i);
                    }else{
                        animate_out('content_animation',slides[status.current_slide]['animation']['content_animation']['in_mob_anim'],slides[status.current_slide]['animation']['content_animation']['out_mob_anim'],i);
                    }
                }
                var change_slide=function(move){
                    if(move=='prev'){
                        if(status.current_slide>0){
                            status.current_slide--;
                        }else{
                            status.current_slide=slider_array.length-1
                        }
                    }else{
                        if(status.current_slide<slider_array.length-1){
                            status.current_slide++;
                        }else{
                            status.current_slide=0
                        }
                    }
                    animate_status.in_cycle--;
                    for(var i=0; i<options.views_count;i++){
                        setTimeout(start_animation,options.elements_delay*i,i);
                    }
                    setTimeout(function(){animate_status.in_cycle++; test_animate();},options.views_count*options.elements_delay)

                }
                var prev=function(e){
                    e.set_inprogress(1);
                    change_slide('prev');
                };
                var next=function(e){
                    e.set_inprogress(1);
                    change_slide('next');
                };
                var render_base=function(){
                    for(var i=0;i<options.views_count;i++){
                        set_content(status.current_slide,i);
                    }
                }
               var  init=function(){ 
                   if(slides.length>0){
                        status.count_slides=slider_array.length;
                        render_base();
                        console.log(slider_array);
                        lister=lister_type_main({
                            count:status.count_slides,
                            template: $('.-lister_type_friends_template').html(),
                            block: $(this_block).find('.-lister_container'),
                            CallBackPrev: prev,
                            CallBackNext: next
                        });
                    }
                }
                init();
            });
        };
        init_sliders(base_slides);
        return base_return;
     }
