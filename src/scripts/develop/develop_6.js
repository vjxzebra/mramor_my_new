//sliding menu
    var slidingHeaderCounter = 4,
    middleElements=0,

    sliderHeaderMenuReInit = function(sectionName, slidesNumber){
        $(sectionName).find('.sliding-menu .item').remove();
        var template = $(sectionName).find('.-section-header-slider-template').html();
        slidingHeaderCounter = slidesNumber;
        middleElements = slidesNumber/2;
        for(var i = 0; i<slidesNumber; i++){
            $(sectionName).find('.sliding-menu').append(template);
            var currentItem = $(sectionName).find('.hidden-arrey .item').eq(i).find('a').text();
            // $(sectionName).find('.sliding-menu li').append(currentItem);
            $(sectionName).find('.sliding-menu .item').eq(i).find('a').append(currentItem);
            // if(i==middleElements || i==(middleElements-1)){
            //     $(sectionName).find('.sliding-menu .item').eq(i).addClass('center');
            // }
            if(i< middleElements){
                $(sectionName).find('.sliding-menu .item').eq(i).addClass('left-item');
            }else if(i>=middleElements){
                $(sectionName).find('.sliding-menu .item').eq(i).addClass('right-item');
            }
        }
    },

    slidingHeaderMenu = function(sectionName, slidesNumber){
        // slideArray
        var slideArrayCommon  = $(sectionName).find('.hidden-arrey  .item'),
        slideArray = slideArrayCommon,
        slidingHeaderCurrentSlide = $(sectionName).find('.sliding-menu').data('currentSlide'),
        slideArrayLength = slideArray.length,
        slidingHeaderItemArray = $(sectionName).find('.sliding-menu .item'),
        slidesToShow = slidesNumber,
        slidingHeaderCounterLocal = slidesNumber,
        clickFlag = 1,
        template = $(sectionName).find('.-section-header-slider-template').html();
        middleElements = slidesNumber/2;


        for(var i = 0; i<slidesNumber; i++){
            $(sectionName).find('.sliding-menu').append(template);
            var currentItem = $(sectionName).find('.hidden-arrey .item').eq(i).find('a').text();

            $(sectionName).find('.sliding-menu .item').eq(i).find('a').append(currentItem);

            // if(i==middleElements || i==(middleElements-1)){
            //     $(sectionName).find('.sliding-menu .item').eq(i).addClass('center');
            // }

            if(i< middleElements){
                $(sectionName).find('.sliding-menu .item').eq(i).addClass('left-item');
            }else if(i>=middleElements){
                $(sectionName).find('.sliding-menu .item').eq(i).addClass('right-item');
            }

        }

        var animationInFunc = function(elem,animation){
            $(elem).addClass(animation).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function(){
                // $(sectionName).find('.sliding-menu .item').eq(i).find('a').text( $(sectionName).find('.hidden-arrey .item').eq(slidingHeaderCurrentSlide).find('a').text());
                $(this).removeClass(animation);
                clickFlag++;
            })
        }

        var animationOut = function(currentSlideHidden,blockIndex,animationOut, animationIn){
                clickFlag--;
                $(sectionName).find('.sliding-menu .item').eq(blockIndex).addClass(animationOut).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                function(){
                    // $(sectionName).find('.sliding-menu .item').eq(i).find('a').text( $(sectionName).find('.hidden-arrey .item').eq(slidingHeaderCurrentSlide).find('a').text());
                    $(this).removeClass(animationOut);
                    $(this).find('a').text( $(sectionName).find('.hidden-arrey .item').eq(currentSlideHidden).find('a').text());
                    animationInFunc($(this),animationIn);
                })

        }


        $(sectionName).find('.next').on('click', function(){
            if(clickFlag===1){
                slidingHeaderCounterLocal = slidingHeaderCounter;
                slidingHeaderCurrentSlide = $(sectionName).find('.sliding-menu').data('currentSlide');
                if((slidingHeaderCurrentSlide + slidingHeaderCounterLocal) <= (slideArrayLength-slidingHeaderCounterLocal)){
                    slidingHeaderCurrentSlide += slidingHeaderCounterLocal;
                }else if((slidingHeaderCurrentSlide+slidingHeaderCounterLocal)>=(slideArrayLength)){
                    slidingHeaderCurrentSlide=0;
                }else{
                    slidingHeaderCurrentSlide= slideArrayLength -slidingHeaderCounterLocal;
                }
                $(sectionName).find('.sliding-menu').data('currentSlide',slidingHeaderCurrentSlide);



                for(var i =0; i<slidingHeaderCounterLocal; i++){

                    if(i<middleElements){

                        animationOut(slidingHeaderCurrentSlide,i,'animation-slide-out-left','animation-slide-in-left');

                    }else{

                        animationOut(slidingHeaderCurrentSlide,i,'animation-slide-out-right','animation-slide-in-right');

                    }
                    slidingHeaderCurrentSlide++;
                }
            }
        });
        $(sectionName).find('.prev').on('click', function(){
            if(clickFlag){

                slidingHeaderCounterLocal = slidingHeaderCounter;
                slidingHeaderCurrentSlide = $(sectionName).find('.sliding-menu').data('currentSlide');
                if(slidingHeaderCurrentSlide === 0){
                    slidingHeaderCurrentSlide = slideArrayLength - slidingHeaderCounterLocal;
                }else if(slidingHeaderCurrentSlide<=(slidingHeaderCounterLocal-1)){
                    slidingHeaderCurrentSlide = 0;
                } else {
                    slidingHeaderCurrentSlide = slidingHeaderCurrentSlide - (slidingHeaderCounterLocal);
                }
                $(sectionName).find('.sliding-menu').data('currentSlide',slidingHeaderCurrentSlide);

                for(var i =0; i<slidingHeaderCounterLocal; i++){
                    if(i<middleElements){

                        animationOut(slidingHeaderCurrentSlide,i,'animation-slide-out-left','animation-slide-in-left');

                    }else{
                        animationOut(slidingHeaderCurrentSlide,i,'animation-slide-out-right','animation-slide-in-right');

                    }
                    slidingHeaderCurrentSlide++;
                }
            }
        });
    },
// sliding menu

//scroll 1 block button
    scrollOneSection = function(){
        $(document).on('click','.navigate-button', function(e){
            var target = $('.gallery').offset().top;

            $('body,html').stop().animate({scrollTop:target},800);
            return false;


        });
    },
//scroll 1 block button

//galery section construct
    galeryConstruct = function(){
        var galleryItemsArray = $('.gallery_item');
        $('.gallery_list.list1 .gallery_item').remove();
        for(var i =0; i < galleryItemsArray.length; i++){
            if(i<3){
                $('.gallery_list.list1').append(galleryItemsArray[i]);
            }else{
                $('.gallery_list.list2').append(galleryItemsArray[i]);
            }

            if(i == (galleryItemsArray.length-1)){
                $(galleryItemsArray[i]).addClass('last-item');
            }

        }

    },
//galery section construct

// main menu togle open/closed
    main_menu_opened = function(){
        $(document).on('click', '.js_main-menu_open',function(){
            $(document).find('.main-menu').toggleClass('opened');
            $(document).find('.menu-button').toggleClass('js_clicked');
        });
    };

//main menu togle open/closed

//google map

    google.maps.event.addDomListener(window, 'load', init);
    var map, markersArray = [];

    function bindInfoWindow(marker, map, location) {
        google.maps.event.addListener(marker, 'click', function() {
            function close(location) {
                location.ib.close();
                location.infoWindowVisible = false;
                location.ib = null;
            }

            if (location.infoWindowVisible === true) {
                close(location);
            } else {
                markersArray.forEach(function(loc, index){
                    if (loc.ib && loc.ib !== null) {
                        close(loc);
                    }
                });

                var boxText = document.createElement('div');
                boxText.style.cssText = 'background: #fff;';
                boxText.classList.add('md-whiteframe-2dp');

                function buildPieces(location, el, part, icon) {
                    if (location[part] === '') {
                        return '';
                    } else if (location.iw[part]) {
                        switch(el){
                            case 'photo':
                                if (location.photo){
                                    return '<div class="iw-photo" style="background-image: url(' + location.photo + ');"></div>';
                                 } else {
                                    return '';
                                }
                                break;
                            case 'iw-toolbar':
                                return '<div class="iw-toolbar"><h3 class="md-subhead">' + location.title + '</h3></div>';
                                break;
                            case 'div':
                                switch(part){
                                    case 'email':
                                        return '<div class="iw-details"><i class="material-icons" style="color:#4285f4;"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><span><a href="mailto:' + location.email + '" target="_blank">' + location.email + '</a></span></div>';
                                        break;
                                    case 'web':
                                        return '<div class="iw-details"><i class="material-icons" style="color:#4285f4;"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><span><a href="' + location.web + '" target="_blank">' + location.web_formatted + '</a></span></div>';
                                        break;
                                    case 'desc':
                                        return '<label class="iw-desc" for="cb_details"><input type="checkbox" id="cb_details"/><h3 class="iw-x-details">Details</h3><i class="material-icons toggle-open-details"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><p class="iw-x-details">' + location.desc + '</p></label>';
                                        break;
                                    default:
                                        return '<div class="iw-details"><i class="material-icons"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><span>' + location[part] + '</span></div>';
                                    break;
                                }
                                break;
                            case 'open_hours':
                                var items = '';
                                if (location.open_hours.length > 0){
                                    for (var i = 0; i < location.open_hours.length; ++i) {
                                        if (i !== 0){
                                            items += '<li><strong>' + location.open_hours[i].day + '</strong><strong>' + location.open_hours[i].hours +'</strong></li>';
                                        }
                                        var first = '<li><label for="cb_hours"><input type="checkbox" id="cb_hours"/><strong>' + location.open_hours[0].day + '</strong><strong>' + location.open_hours[0].hours +'</strong><i class="material-icons toggle-open-hours"><img src="//cdn.mapkit.io/v1/icons/keyboard_arrow_down.svg"/></i><ul>' + items + '</ul></label></li>';
                                    }
                                    return '<div class="iw-list"><i class="material-icons first-material-icons" style="color:#4285f4;"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><ul>' + first + '</ul></div>';
                                 } else {
                                    return '';
                                }
                                break;
                         }
                    } else {
                        return '';
                    }
                }

                boxText.innerHTML =
                    buildPieces(location, 'photo', 'photo', '') +
                    buildPieces(location, 'iw-toolbar', 'title', '') +
                    buildPieces(location, 'div', 'address', 'location_on') +
                    buildPieces(location, 'div', 'web', 'public') +
                    buildPieces(location, 'div', 'email', 'email') +
                    buildPieces(location, 'div', 'tel', 'phone') +
                    buildPieces(location, 'div', 'int_tel', 'phone') +
                    buildPieces(location, 'open_hours', 'open_hours', 'access_time') +
                    buildPieces(location, 'div', 'desc', 'keyboard_arrow_down');

                var myOptions = {
                    alignBottom: true,
                    content: boxText,
                    disableAutoPan: true,
                    maxWidth: 0,
                    pixelOffset: new google.maps.Size(-140, -40),
                    zIndex: null,
                    boxStyle: {
                        opacity: 1,
                        width: '280px'
                    },
                    closeBoxMargin: '0px 0px 0px 0px',
                    infoBoxClearance: new google.maps.Size(1, 1),
                    isHidden: false,
                    pane: 'floatPane',
                    enableEventPropagation: false
                };

                location.ib = new InfoBox(myOptions);
                location.ib.open(map, marker);
                location.infoWindowVisible = true;
            }
        });
    }

    function init() {
        var mapOptions = {
            center: new google.maps.LatLng(55.6806818844019,36.60552873828124),
            zoom: 7,
            gestureHandling: 'auto',
            fullscreenControl: false,
            zoomControl: true,
            disableDoubleClickZoom: true,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            },
            scaleControl: true,
            scrollwheel: false,
            streetViewControl: true,
            draggable : true,
            clickableIcons: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"},{"visibility":"on"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"on"},{"hue":"#ff0000"},{"saturation":"-100"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#a9a9a9"}]}]
        }
        var mapElement = document.getElementById('map');
        map = new google.maps.Map(mapElement, mapOptions);
        var locations = [
            {"title":"Москва","address":"Москва, Россия","desc":"","tel":"","int_tel":"","email":"","web":"","web_formatted":"","open":"","time":"","lat":55.755826,"lng":37.6173,"vicinity":"Москва","open_hours":"","marker":{"url":"http://test15.sheep.fish/images/location-simage.png","scaledSize":{"width":59,"height":64,"f":"px","b":"px"},"origin":{"x":0,"y":0},"anchor":{"x":30,"y":64}},"iw":{"address":true,"desc":true,"email":true,"enable":true,"int_tel":true,"open":true,"open_hours":true,"photo":true,"tel":true,"title":true,"web":true}}
        ];
        for (var i = 0; i < locations.length; i++) {
            var marker = new google.maps.Marker({
                icon: locations[i].marker,
                position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
                map: map,
                title: locations[i].title,
                address: locations[i].address,
                desc: locations[i].desc,
                tel: locations[i].tel,
                int_tel: locations[i].int_tel,
                vicinity: locations[i].vicinity,
                open: locations[i].open,
                open_hours: locations[i].open_hours,
                photo: locations[i].photo,
                time: locations[i].time,
                email: locations[i].email,
                web: locations[i].web,
                iw: locations[i].iw
            });
            markersArray.push(marker);

            if (locations[i].iw.enable === true){
                bindInfoWindow(marker, map, locations[i]);
            }
        }
    }


//google map

//adaptation scripts
var flag1280 =true;
var flag992 = true;
var flag767 = true;
var adapatationScripts = function(){

    if($(window).width()<=1279 && flag1280){
            // $('.gallery .list1').append($('.gallery .list2 .gallery_item'));
            flag1280 = false;


    }else if($(window).width()>1279 && !flag1280){
        // var galleryListArrey = $('.gallery .list1 .gallery_item');
        // for(var i=0; i<galleryListArrey.length; i++){
        //     if(i>2){
        //         $('.gallery .list2').append(galleryListArrey[i]);
        //     }
        //
        // }

        flag1280 = true;
    }


    var linlClickFlag = null;
    if($(window).width()<=992){
            $('.thin-border-square>a').on('click',function(e){
                linlClickFlag=$(this).closest('.thin-border-square').attr('data-click-flag');
                $('.thin-border-square[data-click-flag="false"]').attr('data-click-flag',true);
                if(linlClickFlag!='false'){
                    e.preventDefault();
                    $(this).closest('.thin-border-square').attr('data-click-flag',false);
                }else{
                    $(this).closest('.thin-border-square').attr('data-click-flag',true);
                }

            });
    }else if($(window).width()>992){

    }

    var materialsMarckupContainer;
    if($(window).width()<=767 && flag767){
            materialsMarckupContainer = $('.filter-section').detach();
            $('.side-filter-menu').prepend(materialsMarckupContainer);
            flag767 = false;


    }else if($(window).width()>767 && !flag767){
            materialsMarckupContainer = $('.filter-section').detach();
            $('.regular-top-block').after(materialsMarckupContainer);
        flag767 = true;
    }
};


//adaptation scripts

//info popup open

    var infoPopupOpen = function(){
        var hoverflag= true;
        var timeoutFunction;

        $(document).on('mouseenter','.information-wrap', function(e){
            if(hoverflag){
                // e.stopPropagation();
                $(this).find('.info-popup').addClass('visible');
                hoverflag=false;
            }
        });
        $(document).on('mouseleave','.information-wrap', function(){
            if(hoverflag){
                $('.info-popup').removeClass('visible');
            }else{
                timeoutFunction = setTimeout(function(){
                    var curretflag = hoverflag;
                    hoverflag=true;
                    $('.info-popup').removeClass('visible');
                },300);
            }
        });
        $(document).on('mouseenter','.info-popup', function(){
            hoverflag = true;
            clearTimeout(timeoutFunction);
        });
        $(document).on('mouseleave','.info-popup', function(){
            hoverflag = true;
            $('.info-popup').removeClass('visible');
        });

    },
//info popup open

//set section height according to window width

    setSectionHeight = function(sectionName){

        var windowWidthCurrent =  $(window).width();
        var wimdowheight =  $(window).height();
        var windowHEightTopDiapason = wimdowheight+50;
        var windowHEightBottomDiapason = wimdowheight-50;
        var correlation = 1.77;
        var sectionHeight = windowWidthCurrent/correlation;
        if(sectionHeight< windowHEightTopDiapason && sectionHeight > windowHEightBottomDiapason){
            $(sectionName).outerHeight(wimdowheight);
        }

    },
//

//main menu button scroll
    mainMenuButtonScroll = function(){
        var chackingSectionOnScroll = function(){
            currentPosition = $('.menu-button').offset().top;
            for(var i= 0; i< $('.js_light-section').length; i++){
                if(currentPosition > $('.js_light-section').eq(i).offset().top && currentPosition < ($('.js_light-section').eq(i).offset().top + $('.js_light-section').eq(i).outerHeight()) ){
                    lightSectionPosition = $('.js_light-section').eq(i).offset().top;
                    lightSectionHeight = $('.js_light-section').eq(i).offset().top + $('.js_light-section').eq(i).outerHeight();
                    if(collorChangeFlag){
                        $('.menu-button div').css('backgroundColor','rgba(0,0,0,1)');
                        $('.menu-button .background').css('backgroundColor','rgba(255,255,255,1)');
                        collorChangeFlag=false;
                        scrollingFlag = true;
                    }
                }
            }

            if(currentPosition < lightSectionPosition || currentPosition > lightSectionHeight){
                if(scrollingFlag){
                    $('.menu-button div').css('backgroundColor','rgba(255,255,255,1)');
                    $('.menu-button .background').css('backgroundColor','rgba(0,0,0,1)');
                    scrollingFlag= false;
                    collorChangeFlag =true;
                }
            }
        }
        if($('.js_light-section').length){
            var currentPosition = null,
            scrollingFlag= true,
            collorChangeFlag = true,
            lightSectionPosition = $('.js_light-section').eq(0).offset().top,
            lightSectionHeight = $('.js_light-section').eq(0).offset().top + $('.js_light-section').eq(1).outerHeight();

            chackingSectionOnScroll();

            $(document).on('scroll',function(){
                chackingSectionOnScroll();
            });
        }
    };
//main menu button scroll

//side menu bar .toggle-side_menu-button scroll
    var side_menu_buttonScroll = function(){
        if($('.item_container-section').length){
            var containerPosition = $('.item_container-section').offset().top;
            $('.toggle-side_menu-button span').css('top','50vh');
            var buttonStartPosition = $('.toggle-side_menu-button span').position().top;
            var currentPosition = null;
            var halfWindowHeight = $(window).height()/2;
            var buttonScrollPosition = null;
            $(document).on('scroll',function(){
                halfWindowHeight = $(window).height()/2;
                currentPosition = $(window).scrollTop();
                containerPosition = $('.item_container-section').offset().top;
                buttonScrollPosition = buttonStartPosition+75+(currentPosition-containerPosition);
                if(currentPosition > containerPosition && currentPosition < (containerPosition + $('.item_container-section').outerHeight()-200- halfWindowHeight)) {
                    $('.toggle-side_menu-button span').css('top',buttonScrollPosition);
                }else if(currentPosition > (containerPosition + $('.item_container-section').outerHeight()- 200 - halfWindowHeight)){

                }else{
                    $('.toggle-side_menu-button span').css('top','50vh');
                    buttonStartPosition = $('.toggle-side_menu-button span').position().top;
                }
            });
        }

    };
//side menu bar .toggle-side_menu-button scroll

//sliding side venu
    var slidingSideVenu = function(){
        $('.toggle-side_menu-button').on('click', function(){
            $(this).closest('.side-filter-menu').toggleClass('visible');
        });
    };
//sliding side venu

//define list item in
 var defineLastItem = function(){

     var itemList = $('.js_five-in-line .thin-border-square');

     for(var l = 0; l< itemList.length; l++){
         if(0===l%5 && l!==0){
             itemList.eq(l-1).addClass('lastItem');
         }

     }
 };

//define list item in

//change materials page items order
    var changeOreder = function(){
            $('.item_container-section .inner-wrap .five-in-line').addClass('visible');
            $('.js_many-in-line').addClass('active');

            $('.js_many-in-line').on('click',function(){
                $('.js_one-in-line').removeClass('active');
                $(this).addClass('active')
                $('.item_container-section .inner-wrap>ul').removeClass('visible');
                $('.item_container-section .inner-wrap .five-in-line').addClass('visible');
            });

            $('.js_one-in-line').on('click',function(){
                $('.js_many-in-line').removeClass('active');
                $(this).addClass('active')
                $('.item_container-section .inner-wrap>ul').removeClass('visible');
                $('.item_container-section .inner-wrap .one-in-line').addClass('visible');
            });
    };
//change materials page items order


$(document).ready(function(){
    slidingHeaderMenu('.gallery',4);
    slidingHeaderMenu('.products',4);
    // galeryConstruct();
    main_menu_opened();
    AOS.init();
    adapatationScripts();
    setTimeout(function(){
        AOS.refresh();
    },1000);
    scrollOneSection();

    if($(window).width()<768){
        sliderHeaderMenuReInit('.gallery',2);
        sliderHeaderMenuReInit('.products',2);
    }
    infoPopupOpen();

    if($(window).width()>1280){
        setSectionHeight('.slider_type_main');
    }

    mainMenuButtonScroll();

    slidingSideVenu();
    defineLastItem();

    changeOreder();
    side_menu_buttonScroll();

});
$(window).load(function(){

});

$(window).resize(function(){

    adapatationScripts();
    if($(window).width()<768){
        sliderHeaderMenuReInit('.gallery',2);
        sliderHeaderMenuReInit('.products',2);
    }else {
        sliderHeaderMenuReInit('.gallery',4);
        sliderHeaderMenuReInit('.products',4);
    }

    if($(window).width()>1280){
        setSectionHeight('.slider_type_main');
    }
});
